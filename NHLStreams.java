
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import static java.lang.Thread.sleep;
import java.security.GeneralSecurityException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Nick
 */
public class NHLStreams extends javax.swing.JFrame {

    FindURL f = new FindURL(getPSTDate());
    FindURL f2;
    String os, url = "";
    String buf = null, out = "Failed";
    private String pw;
    Prop p;
    private boolean vlcdone = false, hostEdited = false;

    private void checkHostFile() {
        File hostsFile;
        if (os.contains("mac")) {
            hostsFile = new File("/private/etc/hosts");
        } else if (os.contains("windows")) {
            hostsFile = new File("C:\\Windows\\System32\\drivers\\etc\\hosts");
        } else {
            hostsFile = new File("/etc/hosts");
        }

        boolean foundLine = false;

        try {
            try ( //Construct BufferedReader from InputStreamReader
                    FileInputStream fis = new FileInputStream(hostsFile); BufferedReader br = new BufferedReader(new InputStreamReader(fis))) {
                String line;
                while ((line = br.readLine()) != null) {
                    if (line.contains("127.0.0.1 nlsk.neulion.com") || line.contains("127.0.0.1\tnlsk.neulion.com")) {
                        foundLine = true;
                        br.close();
                        break;
                    }
                }
            }

            if (!foundLine) {
                hostLbl.setText("Host file not edited.");
                hostEdited = false;
            } else {
                hostLbl.setText("Host file edited.");
                hostEdited = true;
            }
            if (!foundLine && os.toLowerCase().contains("win")) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Would you like me to edit your Host file?", "Edit Hosts", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    try (PrintWriter o = new PrintWriter(new BufferedWriter(new FileWriter(hostsFile, true)))) {
                        o.println("\n127.0.0.1 nlsk.neulion.com");
                        o.close();
                        hostLbl.setText("Host file edited.");
                        hostEdited = true;
                    } catch (IOException e) {

                        e.printStackTrace();
                    }
                }

            }
            if (!foundLine && !os.toLowerCase().contains("win")) {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Would you like me to edit your Host file?", "", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    StartPrograms s = new StartPrograms(os);
                    s.editHost(pw);
                    hostLbl.setText("Host file edited.");
                    hostEdited = true;
                }

            }

        } catch (IOException e) {
            hostLbl.setText("Host file not edited.");
            hostEdited = false;
            e.printStackTrace();
        }
    }

    /**
     * Creates new form NHLStreams
     *
     * @throws java.security.GeneralSecurityException
     */
    public NHLStreams() throws GeneralSecurityException, UnsupportedEncodingException, IOException {
        initComponents();
        this.setLocationRelativeTo(null);
        os = System.getProperty("os.name").toLowerCase();
        p = new Prop(os);

        if (!os.toLowerCase().contains("win")) {
            if (p.getPW().equals("")) {
                String p1 = null, p2;
                while (true) {
                    JPasswordField pf = new JPasswordField();
                    pf.addHierarchyListener(new RequestFocusListener());
                    JPanel pPanel = new JPanel();
                    pPanel.setLayout(new BoxLayout(pPanel, BoxLayout.Y_AXIS));
                    pPanel.add(new JLabel("Enter your sudo password"));
                    pPanel.add(pf);
                    JPasswordField pf2 = new JPasswordField();
                    pf2.addHierarchyListener(new RequestFocusListener());
                    JPanel pPanel2 = new JPanel();
                    pPanel2.setLayout(new BoxLayout(pPanel2, BoxLayout.Y_AXIS));
                    pPanel2.add(new JLabel("Enter your sudo password again"));
                    pPanel2.add(pf2);
                    int okCxl = JOptionPane.showConfirmDialog(null, pPanel, "Sudo", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                    if (okCxl == JOptionPane.OK_OPTION) {
                        p1 = new String(pf.getPassword());
                    } else {
                        System.exit(0);
                    }

                    int okCxl2 = JOptionPane.showConfirmDialog(null, pPanel2, "Sudo", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

                    if (okCxl2 == JOptionPane.OK_OPTION) {
                        p2 = new String(pf2.getPassword());
                    } else {
                        continue;
                    }

                    if (!p1.isEmpty() && (p1 == null ? p2 == null : p1.equals(p2))) {
                        pw = p1;
                        int dialogButton = JOptionPane.YES_NO_OPTION;
                        int dialogResult = JOptionPane.showConfirmDialog(null, "Would you like me to save your password?", "Save Password", dialogButton);
                        if (dialogResult == JOptionPane.YES_OPTION) {
                            Encryption e = new Encryption("5rudh=wrh");
                            p.setPW(e.encrypt(pw));
                        }
                        break;
                    }

                    JOptionPane.showMessageDialog(null, "Passwords don't match. Try again.");
                }
            } else {
                Encryption e = new Encryption("5rudh=wrh");
                pw = e.decrypt(p.getPW());
            }
        } else {
            pw = "";
        }
        checkHostFile();
        updateJARLabel();
        updateVLCLabel();
        updateBitrate();
        updateCDN();
        try {
            int i;
            i = f.search();

            DefaultListModel model = new DefaultListModel();
            if (i > 0) {
                ArrayList games = f.getTdyGms();

                for (Object game : games) {
                    model.addElement(game);
                }

                gameList.setModel(model);

            } else {
                model.addElement("No games today!");
                gameList.setModel(model);
                playBtn.setEnabled(false);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jRadioButtonMenuItem1 = new javax.swing.JRadioButtonMenuItem();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        fileChooser = new javax.swing.JFileChooser();
        jMenuItem1 = new javax.swing.JMenuItem();
        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        buttonGroup6 = new javax.swing.ButtonGroup();
        buttonGroup7 = new javax.swing.ButtonGroup();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        setVLCBtn = new javax.swing.JButton();
        fnTA = new java.awt.TextArea();
        setFNBtn = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        frCB = new javax.swing.JCheckBox();
        hostLbl = new javax.swing.JLabel();
        cdnllnwnlRdBtn = new javax.swing.JRadioButton();
        cdnl3nlRdBtn = new javax.swing.JRadioButton();
        RdBtn5000 = new javax.swing.JRadioButton();
        cdnakRdBtn = new javax.swing.JRadioButton();
        vlcLbl = new javax.swing.JLabel();
        jarLbl = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        gameList = new javax.swing.JList();
        RdBtn4500 = new javax.swing.JRadioButton();
        RdBtn800 = new javax.swing.JRadioButton();
        homeRdBtn = new javax.swing.JRadioButton();
        awayRdBtn = new javax.swing.JRadioButton();
        playBtn = new javax.swing.JButton();
        RdBtn3000 = new javax.swing.JRadioButton();
        RdBtn1600 = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        gc1RB = new javax.swing.JRadioButton();
        gcam2RB = new javax.swing.JRadioButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        vlcLbl1 = new javax.swing.JLabel();
        setVLCBtn1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        gameList1 = new javax.swing.JList();
        homeRdBtn1 = new javax.swing.JRadioButton();
        awayRdBtn1 = new javax.swing.JRadioButton();
        RdBtn801 = new javax.swing.JRadioButton();
        RdBtn1601 = new javax.swing.JRadioButton();
        RdBtn3001 = new javax.swing.JRadioButton();
        RdBtn4501 = new javax.swing.JRadioButton();
        RdBtn5001 = new javax.swing.JRadioButton();
        cdnakRdBtn1 = new javax.swing.JRadioButton();
        cdnllnwnlRdBtn1 = new javax.swing.JRadioButton();
        cdnl3nlRdBtn1 = new javax.swing.JRadioButton();
        playBtn1 = new javax.swing.JButton();
        wholeRB = new javax.swing.JRadioButton();
        contRB = new javax.swing.JRadioButton();
        condRB = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        dateChooser = new datechooser.beans.DateChooserCombo();
        chooseBtn = new javax.swing.JButton();
        frCB2 = new javax.swing.JCheckBox();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        jRadioButtonMenuItem1.setSelected(true);
        jRadioButtonMenuItem1.setText("jRadioButtonMenuItem1");

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("LazyMan 2");
        setFocusCycleRoot(false);
        setResizable(false);

        setVLCBtn.setText("Set");
        setVLCBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                setVLCBtnMouseClicked(evt);
            }
        });

        setFNBtn.setText("Set");
        setFNBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                setFNBtnMouseClicked(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        jLabel3.setText("FNeulion output:");

        jLabel2.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        jLabel2.setText("Set VLC/MPC Path:");

        frCB.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        frCB.setText("French");

        hostLbl.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        hostLbl.setText("Checking Host file...");

        buttonGroup3.add(cdnllnwnlRdBtn);
        cdnllnwnlRdBtn.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        cdnllnwnlRdBtn.setText("cdnllnwnl");
        cdnllnwnlRdBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cdnRBAP(evt);
            }
        });

        buttonGroup3.add(cdnl3nlRdBtn);
        cdnl3nlRdBtn.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        cdnl3nlRdBtn.setText("cdnl3nl");
        cdnl3nlRdBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cdnRBAP(evt);
            }
        });

        buttonGroup2.add(RdBtn5000);
        RdBtn5000.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        RdBtn5000.setText("5000");
        RdBtn5000.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bitrateRdBtnActionPerformed(evt);
            }
        });

        buttonGroup3.add(cdnakRdBtn);
        cdnakRdBtn.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        cdnakRdBtn.setSelected(true);
        cdnakRdBtn.setText("cdnak");
        cdnakRdBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cdnRBAP(evt);
            }
        });

        vlcLbl.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        vlcLbl.setText("jLabel4");

        jarLbl.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        jarLbl.setText("Location already set.");

        gameList.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        gameList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Loading..." };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(gameList);

        buttonGroup2.add(RdBtn4500);
        RdBtn4500.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        RdBtn4500.setText("4500");
        RdBtn4500.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bitrateRdBtnActionPerformed(evt);
            }
        });

        buttonGroup2.add(RdBtn800);
        RdBtn800.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        RdBtn800.setText("800");
        RdBtn800.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bitrateRdBtnActionPerformed(evt);
            }
        });

        buttonGroup1.add(homeRdBtn);
        homeRdBtn.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        homeRdBtn.setText("Home");
        homeRdBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeRdBtnActionPerformed(evt);
            }
        });

        buttonGroup1.add(awayRdBtn);
        awayRdBtn.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        awayRdBtn.setSelected(true);
        awayRdBtn.setText("Away");

        playBtn.setText("Play");
        playBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                playBtnMouseClicked(evt);
            }
        });
        playBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playBtnActionPerformed(evt);
            }
        });

        buttonGroup2.add(RdBtn3000);
        RdBtn3000.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        RdBtn3000.setText("3000");
        RdBtn3000.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bitrateRdBtnActionPerformed(evt);
            }
        });

        buttonGroup2.add(RdBtn1600);
        RdBtn1600.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        RdBtn1600.setSelected(true);
        RdBtn1600.setText("1600");
        RdBtn1600.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bitrateRdBtnActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        jLabel1.setText("Set FNeulion Path:");

        buttonGroup1.add(gc1RB);
        gc1RB.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        gc1RB.setText("G.Cam1");

        buttonGroup1.add(gcam2RB);
        gcam2RB.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        gcam2RB.setText("G.Cam2");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(fnTA, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(awayRdBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(homeRdBtn)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(gc1RB))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(RdBtn3000)
                                    .addComponent(RdBtn800))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(RdBtn1600)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(RdBtn4500)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(RdBtn5000))))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addComponent(frCB)
                                .addGap(18, 18, 18)
                                .addComponent(gcam2RB))
                            .addComponent(playBtn)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cdnakRdBtn)
                                    .addComponent(cdnllnwnlRdBtn)
                                    .addComponent(cdnl3nlRdBtn)))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(setFNBtn))
                            .addComponent(jarLbl))
                        .addGap(37, 37, 37)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(setVLCBtn))
                            .addComponent(vlcLbl))))
                .addContainerGap(23, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(hostLbl)
                .addGap(85, 85, 85))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(setFNBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(4, 4, 4)
                        .addComponent(jarLbl))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(setVLCBtn))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(vlcLbl)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(awayRdBtn)
                            .addComponent(homeRdBtn)
                            .addComponent(gc1RB))
                        .addGap(1, 1, 1)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(frCB)
                            .addComponent(gcam2RB))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(RdBtn800)
                            .addComponent(RdBtn1600))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(RdBtn3000)
                            .addComponent(RdBtn4500)
                            .addComponent(RdBtn5000))
                        .addGap(10, 10, 10)
                        .addComponent(cdnakRdBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cdnllnwnlRdBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cdnl3nlRdBtn)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(playBtn))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3)
                .addGap(18, 18, 18)
                .addComponent(fnTA, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(hostLbl)
                .addGap(45, 45, 45))
        );

        jTabbedPane2.addTab("Live", jPanel2);

        jLabel4.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        jLabel4.setText("Set VLC/MPC Path:");

        vlcLbl1.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        vlcLbl1.setText("jLabel4");

        setVLCBtn1.setText("Set");
        setVLCBtn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                setVLCBtn1MouseClicked(evt);
            }
        });

        gameList1.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
        gameList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Choose a date" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(gameList1);

        buttonGroup4.add(homeRdBtn1);
        homeRdBtn1.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        homeRdBtn1.setText("Home");
        homeRdBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                homeRdBtn1ActionPerformed(evt);
            }
        });

        buttonGroup4.add(awayRdBtn1);
        awayRdBtn1.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        awayRdBtn1.setSelected(true);
        awayRdBtn1.setText("Away");

        buttonGroup5.add(RdBtn801);
        RdBtn801.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        RdBtn801.setText("800");

        buttonGroup5.add(RdBtn1601);
        RdBtn1601.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        RdBtn1601.setSelected(true);
        RdBtn1601.setText("1600");

        buttonGroup5.add(RdBtn3001);
        RdBtn3001.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        RdBtn3001.setText("3000");

        buttonGroup5.add(RdBtn4501);
        RdBtn4501.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        RdBtn4501.setText("4500");

        buttonGroup5.add(RdBtn5001);
        RdBtn5001.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        RdBtn5001.setText("5000");

        buttonGroup6.add(cdnakRdBtn1);
        cdnakRdBtn1.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        cdnakRdBtn1.setSelected(true);
        cdnakRdBtn1.setText("cdnak");

        buttonGroup6.add(cdnllnwnlRdBtn1);
        cdnllnwnlRdBtn1.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        cdnllnwnlRdBtn1.setText("cdnllnwnl");

        buttonGroup6.add(cdnl3nlRdBtn1);
        cdnl3nlRdBtn1.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        cdnl3nlRdBtn1.setText("cdnl3nl");

        playBtn1.setText("Play");
        playBtn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                playBtn1MouseClicked(evt);
            }
        });
        playBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playBtn1ActionPerformed(evt);
            }
        });

        buttonGroup7.add(wholeRB);
        wholeRB.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        wholeRB.setSelected(true);
        wholeRB.setText("Whole");

        buttonGroup7.add(contRB);
        contRB.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        contRB.setText("Continuous");

        buttonGroup7.add(condRB);
        condRB.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        condRB.setText("Condensed");

        jLabel5.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        jLabel5.setText("Choose Date:");

        dateChooser.setNothingAllowed(false);
        dateChooser.setFormat(1);
        dateChooser.setMinDate(new java.util.GregorianCalendar(2011, 9, 1));

        chooseBtn.setText("Choose");
        chooseBtn.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chooseBtnMouseClicked(evt);
            }
        });

        frCB2.setFont(new java.awt.Font("Monospaced", 0, 11)); // NOI18N
        frCB2.setText("French");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(RdBtn5001)
                        .addGap(131, 131, 131))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(247, 247, 247)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(RdBtn801)
                            .addComponent(RdBtn3001))
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(RdBtn1601, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(83, 83, 83))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(RdBtn4501)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addGap(64, 64, 64))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(awayRdBtn1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(homeRdBtn1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(frCB2))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cdnllnwnlRdBtn1)
                                    .addComponent(cdnakRdBtn1)
                                    .addComponent(cdnl3nlRdBtn1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(condRB)
                                    .addComponent(wholeRB)
                                    .addComponent(contRB)))))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(playBtn1)))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(setVLCBtn1)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(vlcLbl1)))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(dateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(chooseBtn)
                        .addGap(111, 111, 111))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(dateChooser, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(chooseBtn)
                        .addGap(43, 43, 43))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(setVLCBtn1)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(vlcLbl1)
                        .addGap(45, 45, 45)))
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(awayRdBtn1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(homeRdBtn1)
                            .addComponent(frCB2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(RdBtn1601)
                            .addComponent(RdBtn801, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(RdBtn3001)
                            .addComponent(RdBtn4501))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(RdBtn5001, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cdnakRdBtn1)
                            .addComponent(wholeRB))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cdnllnwnlRdBtn1)
                            .addComponent(contRB))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cdnl3nlRdBtn1)
                            .addComponent(condRB))
                        .addGap(18, 18, 18)
                        .addComponent(playBtn1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(159, Short.MAX_VALUE))))
        );

        jTabbedPane2.addTab("Replays", jPanel3);

        jMenu1.setText("File");

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Close");
        jMenuItem2.setFocusCycleRoot(true);
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem2ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("About");
        jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu2MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 462, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 9, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 510, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        getAccessibleContext().setAccessibleDescription("");

        setBounds(0, 0, 487, 569);
    }// </editor-fold>//GEN-END:initComponents

    private void homeRdBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeRdBtnActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_homeRdBtnActionPerformed

    private void playBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playBtnActionPerformed
        // TODO add your handling code here:


    }//GEN-LAST:event_playBtnActionPerformed

    private void setFNBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_setFNBtnMouseClicked
        // TODO add your handling code here:
        String loc;
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "JAR Files", "jar");
        fileChooser.setFileFilter(filter);
        int returnVal = fileChooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            loc = fileChooser.getSelectedFile().getPath();
            p.setJarloc(loc);
            jarLbl.setText("Location set.");
        }
    }//GEN-LAST:event_setFNBtnMouseClicked

    private void playBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_playBtnMouseClicked
        // TODO add your handling code here:
        fnTA.setText("");
        boolean beforeGameTime = true;
        boolean gameRowSelected = true;
        String message;
        Timezone tz = new Timezone();
        try {
            if (tz.compareTime2(tz.getDate(), f.GetTime(gameList.getSelectedIndex()))) {
                beforeGameTime = false;
            }
        } catch (ParseException ex) {
            beforeGameTime = false;
            message = "Please wait 20 minutes prior to game time.";
            Logger.getLogger(NHLStreams.class.getName()).log(Level.SEVERE, message, ex);
            
            int mc = JOptionPane.ERROR_MESSAGE;
            JOptionPane.showMessageDialog(null, message, "Error", mc);
        } catch (ArrayIndexOutOfBoundsException ex2) {
            gameRowSelected = false;
            message = "Please select a game to watch.";
            Logger.getLogger(NHLStreams.class.getName()).log(Level.INFO, message);
            
            int mc = JOptionPane.ERROR_MESSAGE;
            JOptionPane.showMessageDialog(null, message, "Error", mc);
        }
        if (beforeGameTime && gameRowSelected) {
            String locjar, locvlc, ha = "", gid, team = "";
            String[] u = null;
            vlcdone = false;
            int i = 0, uLen = 0, index = 0;
            boolean manual = false;
            locjar = getJARLoc();
            locvlc = getVLCLoc(os);
            gid = f.getGameID(gameList.getSelectedIndex());
            if (gid != null) {
                if (homeRdBtn.isSelected()) {
                    ha = "home";
                    team = f.GetHomeTeam(gameList.getSelectedIndex());
                    index = 1;
                } else if (awayRdBtn.isSelected()) {
                    ha = "away";
                    team = f.GetAwayTeam(gameList.getSelectedIndex());
                    index = 0;
                } else if (gc1RB.isSelected()) {
                    ha = "gcam1";
                    team = "none";
                } else if (gcam2RB.isSelected()) {
                    ha = "gcam2";
                    team = "none";
                }
                if (frCB.isSelected()) {
                    ha = "french";
                }

                StartPrograms sp = new StartPrograms(os);
                ProcessBuilder jar = null;
                try {
                    jar = sp.StartJAR(locjar, gid, ha, pw);
                } catch (IOException ex) {
                    Logger.getLogger(NHLStreams.class.getName()).log(Level.SEVERE, null, ex);
                }

                SwingWorker<Void, Void> worker;
                worker = restartJAR(jar);

                worker.execute();
                try {
                    sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(NHLStreams.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    if (!frCB.isSelected() && !f.getM3U8(gid)[0].equals("0") && url.equals("") && !team.equals("none")) {
                        url = f.getM3U8(gid)[index];
                    } else if (!frCB.isSelected() && f.getM3U8(gid)[0].equals("0")) {
                        JOptionPane.showMessageDialog(null, "The stream source is down - manually searching for streams.\npsst! Click OK!");
                        team = extendAbbr(team);
                        u = getFrenchURL(team, 0, null);
                        uLen = u.length - 1;
                        manual = true;
                    }
                    if (frCB.isSelected() && url.equals("") && !team.equals("none")) {

                        team = extendAbbr(team);
                        if (team.contains("canadiens") || team.contains("mapleleafs") || team.contains("senators")) {
                            JOptionPane.showMessageDialog(null, "Searching for French streams. Sit tight.\npsst! Click OK!");
                            u = getFrenchURL(team, 1, null);
                            uLen = u.length - 1;
                            manual = true;
                        }
                    }

                    while (i <= uLen) {
                        vlcdone = false;
                        if (uLen > 0) {
                            url = u[i];
                        }
                        if (url == null) {
                            break;
                        }
                        if (!url.equals("") && !locjar.equals("") && !locvlc.equals("") && hostEdited) {

                            if (!worker.isDone()) {
                                if (i == 0 && manual) {
                                    u = getFrenchURL(null, 2, u);
                                    if (u[i] != null) {
                                        url = u[i];
                                        uLen = u.length;
                                    }
                                }
                                url = changeURLPrefs(url);
                                fnTA.append("\nOpening VLC in 3 seconds\n\n");
                                sleep(3000);
                                String args = determineArgs(os, url);
                                ProcessBuilder vlc = sp.StartVLC(locvlc, args);
                                SwingWorker<Void, Void> worker2 = startVLC(vlc);
                                worker2.execute();
                            } else {
                                int mc = JOptionPane.WARNING_MESSAGE;

                                JOptionPane.showMessageDialog(null, "FNeulion quit prematurely. Check its output.", "Error", mc);
                                if (manual) {
                                    break;
                                }
                            }

                        } else if (url.contains("mp4")) {
                            int mc = JOptionPane.WARNING_MESSAGE;
                            JOptionPane.showMessageDialog(null, "The game is over.", "Error", mc);
                            vlcdone = true;
                        } else if (locjar.equals("")) {
                            int mc = JOptionPane.ERROR_MESSAGE;
                            JOptionPane.showMessageDialog(null, "Please set FNeulion path.", "Error", mc);
                            vlcdone = true;
                        } else if (locvlc.equals("")) {
                            int mc = JOptionPane.ERROR_MESSAGE;
                            JOptionPane.showMessageDialog(null, "Please set VLC path.", "Error", mc);
                            vlcdone = true;
                        } else if (!hostEdited) {
                            int mc = JOptionPane.ERROR_MESSAGE;
                            JOptionPane.showMessageDialog(null, "Please edit Host file.", "Error", mc);
                            vlcdone = true;
                        } else {
                            int mc = JOptionPane.WARNING_MESSAGE;
                            JOptionPane.showMessageDialog(null, "No streams found.", "Error", mc);
                            i = uLen + 1;
                            vlcdone = true;
                        }
                        if (uLen > 0 && i <= uLen && manual) {
                            int dialogButton = JOptionPane.YES_NO_OPTION;
                            int dialogResult = JOptionPane.showConfirmDialog(null, "Was it the right stream link?", "Right Link?", dialogButton);
                            if (dialogResult == JOptionPane.YES_OPTION) {
                                i = uLen + 1;
                            }
                        }
                        i++;
                    }

                } catch (IOException | InterruptedException ex) {
                    Logger.getLogger(NHLStreams.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        url = "";
    }//GEN-LAST:event_playBtnMouseClicked

    private void setVLCBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_setVLCBtnMouseClicked
        // TODO add your handling code here:
        String loc;

        fileChooser.setFileFilter(null);

        int returnVal = fileChooser.showOpenDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            loc = fileChooser.getSelectedFile().getPath();

            if (loc.toLowerCase().contains("vlc") || loc.toLowerCase().contains("mpc")) {
                p.setVlcloc(loc);
                vlcLbl.setText("Location set.");
                vlcLbl1.setText("Location set.");
            } else {
                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "This doesn't look like a VLC or MPC-HC path. Set anyway?", "", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {
                    p.setVlcloc(loc);
                    vlcLbl.setText("Location set.");
                    vlcLbl1.setText("Location set.");
                } else {
                    vlcLbl.setText("Location not set.");
                    vlcLbl1.setText("Location not set.");
                }
            }
        }
    }//GEN-LAST:event_setVLCBtnMouseClicked

    private void jMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem2ActionPerformed
        // TODO add your handling code here:
        System.out.println("Exiting...");
        System.exit(0);
    }//GEN-LAST:event_jMenuItem2ActionPerformed

    private void jMenu2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu2MouseClicked
        int mc = JOptionPane.INFORMATION_MESSAGE;
        JOptionPane.showMessageDialog(null, "Ver. 2.0b3r4", "About", mc);
    }//GEN-LAST:event_jMenu2MouseClicked

    private void chooseBtnMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chooseBtnMouseClicked
        // TODO add your handling code here:
        playBtn1.setEnabled(true);
        try {
            int i;
            f2 = new FindURL(getDate(dateChooser.getSelectedDate()));
            i = f2.search();

            DefaultListModel model = new DefaultListModel();
            if (i > 0) {
                ArrayList games = f2.getTdyGms();

                for (Object game : games) {
                    model.addElement(game);
                }

                gameList1.setModel(model);

            } else {
                model.addElement("No games today!");
                gameList1.setModel(model);
                playBtn1.setEnabled(false);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }//GEN-LAST:event_chooseBtnMouseClicked

    private void playBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playBtn1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_playBtn1ActionPerformed

    private void playBtn1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_playBtn1MouseClicked
        // TODO add your handling code here:
        String locvlc, gid, url = null;
        locvlc = getVLCLoc(os);
        gid = f2.getGameID(gameList1.getSelectedIndex());
        if (gid != null) {
            if (homeRdBtn1.isSelected()) {
                url = f2.getReplayM3U8(gid)[1];
            } else if (awayRdBtn1.isSelected()) {
                url = f2.getReplayM3U8(gid)[0];
            }
            if (!url.equals("") && !locvlc.equals("")) {
                try {
                    StartPrograms sp = new StartPrograms(os);
                    url = changeURLPrefs2(url);

                    String args = determineArgs(os, url);
                    ProcessBuilder vlc = sp.StartVLC(locvlc, args);
                    SwingWorker<Void, Void> worker2 = startVLC(vlc);
                    worker2.execute();
                } catch (IOException ex) {
                    Logger.getLogger(NHLStreams.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else if (locvlc.equals("")) {
                int mc = JOptionPane.ERROR_MESSAGE;
                JOptionPane.showMessageDialog(null, "Please set VLC path.", "Error", mc);
            } else {
                int mc = JOptionPane.WARNING_MESSAGE;
                JOptionPane.showMessageDialog(null, "No streams found.", "Error", mc);
            }

            playBtn.setEnabled(true);
            playBtn.setText("Play");

        }
    }//GEN-LAST:event_playBtn1MouseClicked

    private void homeRdBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_homeRdBtn1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_homeRdBtn1ActionPerformed

    private void setVLCBtn1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_setVLCBtn1MouseClicked
        // TODO add your handling code here:
        setVLCBtnMouseClicked(evt);
    }//GEN-LAST:event_setVLCBtn1MouseClicked

    private void bitrateRdBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bitrateRdBtnActionPerformed
        String bitrate = "";
        if (RdBtn800.isSelected()) {
            bitrate = "800";
        } else if (RdBtn1600.isSelected()) {
            bitrate = "1600";
        } else if (RdBtn3000.isSelected()) {
            bitrate = "3000";
        } else if (RdBtn4500.isSelected()) {
            bitrate = "4500";
        } else if (RdBtn5000.isSelected()) {
            bitrate = "5000";
        }
        p.setBitrate(bitrate);
    }//GEN-LAST:event_bitrateRdBtnActionPerformed

    private void cdnRBAP(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cdnRBAP
        // TODO add your handling code here:
        String cdn = "";

        if (cdnakRdBtn.isSelected()) {
            cdn = "cdnak";
        } else if (cdnl3nlRdBtn.isSelected()) {
            cdn = "cdnl3nl";
        } else if (cdnllnwnlRdBtn.isSelected()) {
            cdn = "cdnllnwnl";
        }

        p.setCDN(cdn);
    }//GEN-LAST:event_cdnRBAP

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                try {
                    new NHLStreams().setVisible(true);
                } catch (GeneralSecurityException ex) {
                    Logger.getLogger(NHLStreams.class.getName()).log(Level.SEVERE, null, ex);
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(NHLStreams.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(NHLStreams.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
        // new NHLStreams();

        /* Create and display the form */
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton RdBtn1600;
    private javax.swing.JRadioButton RdBtn1601;
    private javax.swing.JRadioButton RdBtn3000;
    private javax.swing.JRadioButton RdBtn3001;
    private javax.swing.JRadioButton RdBtn4500;
    private javax.swing.JRadioButton RdBtn4501;
    private javax.swing.JRadioButton RdBtn5000;
    private javax.swing.JRadioButton RdBtn5001;
    private javax.swing.JRadioButton RdBtn800;
    private javax.swing.JRadioButton RdBtn801;
    private javax.swing.JRadioButton awayRdBtn;
    private javax.swing.JRadioButton awayRdBtn1;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.ButtonGroup buttonGroup6;
    private javax.swing.ButtonGroup buttonGroup7;
    private javax.swing.JRadioButton cdnakRdBtn;
    private javax.swing.JRadioButton cdnakRdBtn1;
    private javax.swing.JRadioButton cdnl3nlRdBtn;
    private javax.swing.JRadioButton cdnl3nlRdBtn1;
    private javax.swing.JRadioButton cdnllnwnlRdBtn;
    private javax.swing.JRadioButton cdnllnwnlRdBtn1;
    private javax.swing.JButton chooseBtn;
    private javax.swing.JRadioButton condRB;
    private javax.swing.JRadioButton contRB;
    private datechooser.beans.DateChooserCombo dateChooser;
    private javax.swing.JFileChooser fileChooser;
    private java.awt.TextArea fnTA;
    private javax.swing.JCheckBox frCB;
    private javax.swing.JCheckBox frCB2;
    private javax.swing.JList gameList;
    private javax.swing.JList gameList1;
    private javax.swing.JRadioButton gc1RB;
    private javax.swing.JRadioButton gcam2RB;
    private javax.swing.JRadioButton homeRdBtn;
    private javax.swing.JRadioButton homeRdBtn1;
    private javax.swing.JLabel hostLbl;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JLabel jarLbl;
    private javax.swing.JButton playBtn;
    private javax.swing.JButton playBtn1;
    private javax.swing.JButton setFNBtn;
    private javax.swing.JButton setVLCBtn;
    private javax.swing.JButton setVLCBtn1;
    private javax.swing.JLabel vlcLbl;
    private javax.swing.JLabel vlcLbl1;
    private javax.swing.JRadioButton wholeRB;
    // End of variables declaration//GEN-END:variables

    private static String getPSTDate() {
        Timezone tz = new Timezone();

        return tz.getPSTDate();
    }

    private String determineArgs(String os, String url) {
        if (getVLCLoc(os).toLowerCase().contains("vlc") && os.contains("win")) {
            return url + " \":http-user-agent=PS4 libhttp/1.76 (PlayStation 4)\"";
        }
        else if (os.contains("win") || os.contains("mac")) {
            return url;
        }
        else {
            return url + " :http-user-agent=\"PS4 libhttp/1.76 (PlayStation 4)\"";
        }

    }

    private String getJARLoc() {
        String loc;

        loc = p.getJarloc();

        if (loc.equals("")) {
            int mc = JOptionPane.INFORMATION_MESSAGE;
            JOptionPane.showMessageDialog(null, "Please set FNeulion's location.", "Error", mc);
            FileNameExtensionFilter filter = new FileNameExtensionFilter(
                    "JAR Files", "jar");
            fileChooser.setFileFilter(filter);
            int returnVal = fileChooser.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                loc = fileChooser.getSelectedFile().getPath();
                p.setJarloc(loc);
                jarLbl.setText("Location set.");
                return loc;
            } else {
                jarLbl.setText("Location not set.");
                return "";
            }
        } else {
            return loc;
        }

    }

    private String getVLCLoc(String os) {
        String loc;

        loc = p.getVlcloc();

        if (loc.equals("")) {
            if (os.toLowerCase().contains("win")) {
                String path = "C:\\Program Files\\VideoLAN\\VLC\\vlc.exe";
                File file = new File(path);
                if (file.exists()) {
                    p.setVlcloc(path);
                    return path;
                } else {
                    path = "C:\\Program Files (x86)\\VideoLAN\\VLC\\vlc.exe";
                    file = new File(path);
                    if (file.exists()) {
                        p.setVlcloc(path);
                        return path;
                    }
                }
            } else if (os.toLowerCase().contains("mac")) {
                loc = "/Applications/VLC.app/Contents/MacOS/VLC";
                p.setVlcloc(loc);
                return loc;
            } else {
                loc = "/usr/bin/vlc";
                p.setVlcloc(loc);
                return loc;
            }
        } else {
            return loc;
        }
        return "";
    }

    private String changeURLPrefs(String url) {
        String u, cdn = null, bitrate = null;

        if (RdBtn800.isSelected()) {
            bitrate = "800";
        } else if (RdBtn1600.isSelected()) {
            bitrate = "1600";
        } else if (RdBtn3000.isSelected()) {
            bitrate = "3000";
        } else if (RdBtn4500.isSelected()) {
            bitrate = "4500";
        } else if (RdBtn5000.isSelected()) {
            bitrate = "5000";
        }

        if ((gc1RB.isSelected() || gcam2RB.isSelected()) && Integer.parseInt(bitrate) > 1600) {
            bitrate = "1600";
        }

        if (cdnakRdBtn.isSelected()) {
            cdn = "cdnak";
        } else if (cdnl3nlRdBtn.isSelected()) {
            cdn = "cdnl3nl";
        } else if (cdnllnwnlRdBtn.isSelected()) {
            cdn = "cdnllnwnl";
        }

        u = url.replace("ipad", bitrate);
        u = u.replace("cdnak", cdn);

        return u;
    }

    private void getJARStr(Process j) throws IOException {
        String b = null;
        int i = 0;
        try (BufferedReader stdInput = new BufferedReader(new InputStreamReader(j.getInputStream()))) {
            BufferedReader stdError = new BufferedReader(new InputStreamReader(j.getErrorStream()));

            if (stdInput.ready()) {
                //       System.out.println("input");
                try {
                    while (stdInput.ready()) {
                        b = stdInput.readLine();
                        System.out.println(b);
                        if (b.contains("http://nlds")) {
                            url = b;
                            fnTA.append(b + "\n");
                        } else {
                            fnTA.append(b + "\n");
                        }
                        /*i++;
                        if (i == 2 ) {
                            break;
                        }*/
                        //   buf2 = buf2 + buf;
                        //System.out.println("i");
                        // break;
                    }
                } catch (IOException ex) {
                    Logger.getLogger(NHLStreams.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (stdError.ready()) {
                try {
                    while ((b = stdError.readLine()) != null) {
                        fnTA.append(b + "\n");
                        //buf2 = buf2 + buf;
                        //  System.out.println("i");

                    }
                } catch (IOException ex) {
                    Logger.getLogger(NHLStreams.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //System.out.println("690: " + buf2);
            stdError.close();
            b = null;
        }

    }

    private SwingWorker<Void, Void> restartJAR(final ProcessBuilder jar) {

        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {

                Process j = jar.start();

                Thread.sleep(1000);
                getJARStr(j);
                while (true) {
                    try {
                        j.exitValue();
                        System.out.println("jar died");
                        if (!vlcdone) {
                            j = jar.start();
                        } else {

                            break;
                        }

                    } catch (IllegalThreadStateException e) {
                         // System.out.println("Caught. done: " + vlcdone);
                        if (vlcdone) {
                            System.out.println("Killing jar");
                            if (!os.toLowerCase().contains("win")) {
                                StartPrograms.killJAR(pw);
                            }
                            j.destroy();
                            break;
                        }
                        //System.out.println("zzz");
                        Thread.sleep(800);
                     //   getJARStr(j);

                        //System.out.println(buf);
                    }
                }
                if (!os.toLowerCase().contains("win")) {
                    StartPrograms.killJAR(pw);
                }
                System.out.println("jar done");
                return null;
            }
        };
        return worker;
    }

    private SwingWorker<Void, Void> startVLC(final ProcessBuilder vlc) {

        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {

            @Override
            protected Void doInBackground() throws Exception {
                System.out.println("vlc starting...");
                Process v = vlc.start();
                vlcdone = false;
                v.waitFor();
                vlcdone = true;
                System.out.println("vlc done");
                return null;

            }

        };
        return worker;
    }

    private void updateJARLabel() {
        if (!p.getJarloc().equals("")) {
            jarLbl.setText("Location already set.");
        } else {
            jarLbl.setText("Location not set.");
        }
    }

    private void updateVLCLabel() {
        if (!p.getVlcloc().equals("") || !getVLCLoc(os).equals("")) {
            vlcLbl.setText("Location already set.");
            vlcLbl1.setText("Location already set.");
        } else {
            vlcLbl.setText("Location not set.");
            vlcLbl1.setText("Location not set.");
        }
    }

    private String[] getFrenchURL(String team, int type, String[] p) throws IOException, InterruptedException {
        FindFrenchURL french = new FindFrenchURL(team, type);
        if (type == 0 || type == 1) {
            return french.FindURL();
        } else {
            return french.findRightStream(p);
        }
    }

    private String extendAbbr(String team) {
        switch (team) {
            case "OTT":
                return "senators";
            case "TOR":
                return "mapleleafs";
            case "MTL":
                return "canadiens";
            case "NJD":
                return "devils";
            case "ANA":
                return "ducks";
            case "ARI":
                return "coyotes";
            case "BOS":
                return "bruins";
            case "BUF":
                return "sabres";
            case "CAR":
                return "hurricanes";
            case "CBJ":
                return "bluejackets";
            case "CGY":
                return "flames";
            case "CHI":
                return "blackhawks";
            case "COL":
                return "avalanche";
            case "DAL":
                return "stars";
            case "DET":
                return "redwings";
            case "EDM":
                return "oilers";
            case "FLA":
                return "panthers";
            case "LAK":
                return "kings";
            case "MIN":
                return "wild";
            case "NYI":
                return "islanders";
            case "NYR":
                return "rangers";
            case "NSH":
                return "predators";
            case "PHI":
                return "flyers";
            case "PIT":
                return "penguins";
            case "SJS":
                return "sharks";
            case "STL":
                return "blues";
            case "TBL":
                return "lightning";
            case "VAN":
                return "canucks";
            case "WSH":
                return "capitals";
            case "WPG":
                return "jets";

            default:
                return "";
        }
    }

    private String getDate(Calendar text) throws ParseException {
        Timezone tz = new Timezone();

        return tz.getPSTDate(text);
    }

    private String changeURLPrefs2(String url) {
        String u = "", cdn = null, bitrate = null, type = null;

        if (RdBtn801.isSelected()) {
            bitrate = "800";
        } else if (RdBtn1601.isSelected()) {
            bitrate = "1600";
        } else if (RdBtn3001.isSelected()) {
            bitrate = "3000";
        } else if (RdBtn4501.isSelected()) {
            bitrate = "4500";
        } else if (RdBtn5001.isSelected()) {
            bitrate = "5000";
        }

        if (cdnakRdBtn1.isSelected()) {
            cdn = "cdnak";
        } else if (cdnl3nlRdBtn1.isSelected()) {
            cdn = "cdnl3nl";
        } else if (cdnllnwnlRdBtn1.isSelected()) {
            cdn = "cdnllnwnl";
        }

        if (wholeRB.isSelected()) {
            type = "whole";
        } else if (condRB.isSelected()) {
            type = "condensed";
        } else if (contRB.isSelected()) {
            type = "continuous";
        }

        u = url;
        if (u.contains("nlds")) {
            u = u.replace("ipad", bitrate);
            u = u.replace("cdnak", cdn);
        }
        
        if (u.contains("_whole_")) {
            u = u.replace("whole", type);
        } else {
            u = u.replace("condensed", type);
        }

        if (frCB2.isSelected() && (u.contains("mtl") || u.contains("ott") || u.contains("tor"))) {
            u = u.replace("nhl", "nhlfr");
            u = u.replace("_a_", "_fr_");
            u = u.replace("_h_", "_fr_");
        }

        return u;
    }

    private void updateBitrate() {
        String bitrate = p.getBitrate();
        if (bitrate.equals("")) {
        } else if (bitrate.equals("800")) {
            RdBtn800.setSelected(true);
            RdBtn801.setSelected(true);
        } else if (bitrate.equals("1600")) {
            RdBtn1600.setSelected(true);
            RdBtn1601.setSelected(true);
        } else if (bitrate.equals("3000")) {
            RdBtn3000.setSelected(true);
            RdBtn3001.setSelected(true);
        } else if (bitrate.equals("4500")) {
            RdBtn4500.setSelected(true);
            RdBtn4501.setSelected(true);
        } else if (bitrate.equals("5000")) {
            RdBtn5000.setSelected(true);
            RdBtn5001.setSelected(true);
        }
    }

    private void updateCDN() {
        String cdn = p.getCDN();

        switch (cdn) {
            case "cdnak":
                cdnakRdBtn.setSelected(true);
                cdnakRdBtn1.setSelected(true);
                break;
            case "cdnllnwnl":
                cdnllnwnlRdBtn.setSelected(true);
                cdnllnwnlRdBtn1.setSelected(true);
                break;
            case "cdnl3nl":
                cdnl3nlRdBtn.setSelected(true);
                cdnl3nlRdBtn1.setSelected(true);
                break;
            default:
                return;
        }
    }

}
