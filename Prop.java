import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Nick
 */
public class Prop {

    private final Properties prop;
    private String c;
    public Prop(String os) throws IOException {
        prop = new Properties();
        char slash='/';
        if (os.contains("win"))
            slash='\\';
        c=System.getProperty("user.dir")+slash+"config.properties";
    }

    /**
     * @return the vlcloc
     */
    public String getVlcloc() {
        try {
            InputStream input = null;
            input = new FileInputStream(c);

            // load a properties file
            prop.load(input);
            input.close();
            if (prop.getProperty("VLCLocation") != null) {
                return prop.getProperty("VLCLocation");
            } else {
                return "";
            }
        } catch (IOException e) {
            return "";
        }
    }

    /**
     * @param vlcloc the vlcloc to set
     */
    public void setVlcloc(String vlcloc) {
        OutputStream output = null;
        try {
            output = new FileOutputStream(c);

            // set the properties value
            prop.setProperty("VLCLocation", vlcloc);
            prop.store(output, "");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    /**
     * @return the jarloc
     */
    public String getJarloc() {
        try {
            InputStream input = null;
            input = new FileInputStream(c);

            // load a properties file
            prop.load(input);
            input.close();
            if (prop.getProperty("JARLocation") != null) {
                return prop.getProperty("JARLocation");
            } else {
                return "";
            }
        } catch (IOException e) {
            return "";
        }
    }

    /**
     * @param jarloc the jarloc to set
     */
    
    public void setJarloc(String jarloc) {
        OutputStream output = null;
        try {
            output = new FileOutputStream(c);

            // set the properties value
            prop.setProperty("JARLocation", jarloc);
            prop.store(output, "");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
    
    public String getPW() {
        try {
            InputStream input = null;
            input = new FileInputStream(c);

            // load a properties file
            prop.load(input);
            input.close();
            if (prop.getProperty("Pass") != null) {
                return prop.getProperty("Pass");
            } else {
                return "";
            }
        } catch (IOException e) {
            return "";
        }
    }

    /**
     * @param pw
     */
    public void setPW(String pw) {
        OutputStream output = null;
        try {
            output = new FileOutputStream(c);

            // set the properties value
            prop.setProperty("Pass", pw);
            prop.store(output, "");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
    
     public void setBitrate(String br) {
        OutputStream output = null;
        try {
            output = new FileOutputStream(c);

            prop.setProperty("Bitrate", br);
            prop.store(output, "");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
     
     public String getBitrate() {
        try {
            InputStream input = null;
            input = new FileInputStream(c);

            // load a properties file
            prop.load(input);
            input.close();
            if (prop.getProperty("Bitrate") != null) {
                return prop.getProperty("Bitrate");
            } else {
                return "";
            }
        } catch (IOException e) {
            return "";
        }
    }
     
     public void setCDN(String cdn) {
        OutputStream output = null;
        try {
            output = new FileOutputStream(c);

            // set the properties value
           
            
            prop.setProperty("CDN", cdn);
            prop.store(output, "");
            
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }
     
     public String getCDN() {
        try {
            InputStream input = null;
            input = new FileInputStream(c);

            // load a properties file
            prop.load(input);
            input.close();
            if (prop.getProperty("CDN") != null) {
                return prop.getProperty("CDN");
            } else {
                return "";
            }
        } catch (IOException e) {
            return "";
        }
    }

}

