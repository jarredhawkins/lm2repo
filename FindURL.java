
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class FindURL {

    private final String date;
    private final String[] urls;
    private final ArrayList<ArrayList<String>> gameinfo;

    public FindURL(String d) {
        date = d;
        urls = new String[2];
        gameinfo = new ArrayList<ArrayList<String>>(15);
    }

    private String getText(String url) {

        BufferedReader in = null;
        try {
            URL website;
            try {
                website = new URL(url);
            } catch (MalformedURLException j) {
                return "down";
            }
            URLConnection connection = null;
            try {
                connection = website.openConnection();
            } catch (IOException ex) {
                Logger.getLogger(FindURL.class.getName()).log(Level.SEVERE, null, ex);
            }
            in = new BufferedReader(
                    new InputStreamReader(
                            connection.getInputStream()));
            String response = "";
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                response+=inputLine;
            }
            in.close();
            return response;
        } catch (IOException ex) {
            System.err.println(ex);
            return "";
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(FindURL.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public int search() throws Exception {

        JSONParser parser = new JSONParser();
        String jsonText = getText("http://live.nhl.com/GameData/SeasonSchedule-"+getSeason()+".json");
        if (!jsonText.equals("")) {
            Object jsonObject = parser.parse(jsonText);

            JSONArray games = (JSONArray) jsonObject;

            JSONObject t = new JSONObject();

            int i = 0;
            ArrayList row;

            while (i < games.size() - 1) {
                //System.out.print(j.next());
                t = (JSONObject) games.get(i);

                if (t.get("est").toString().substring(0, 8).equals(date)) {
                    row = new ArrayList();
                    row.add(t.get("id").toString());
                    row.add(t.get("est").toString());
                    row.add(t.get("a").toString());
                    row.add(t.get("h").toString());

                    gameinfo.add(row);

                }
                i++;
                
                
            }
            //row.clear();
            if (gameinfo.isEmpty()) {
                return 0;
            } else {
                sort(gameinfo);
                return 1;
            }
        }
        
        return 0;
    }

    public String[] getM3U8(String gid) {
        String[] links = new String[2];
        try {
            
            JSONParser parser = new JSONParser();

            String jsonText = getText("http://smb.cdnak.neulion.com/fs/nhl/mobile/feed_new/data/streams/"+getSeason().substring(0, 4)+"/ipad/" + gid.substring(4, 6) + "_" + gid.substring(6) + ".json");
            if (!jsonText.equals("down") || !jsonText.equals("")) {
                Object t = parser.parse(jsonText);
                JSONObject games = (JSONObject) t;
                t = games.get("gameStreams");
                games = (JSONObject) t;
                t = games.get("ipad");
                games = (JSONObject) t;
                Object t1 = games.get("away");
                JSONObject games2 = (JSONObject) t1;
                t1 = games2.get("live");
                games2 = (JSONObject) t1;
                t1 = games2.get("bitrate0");
                links[0] = String.valueOf(t1);

                t = games.get("home");
                //System.out.println(t);
                games = (JSONObject) t;
                t = games.get("live");
                games = (JSONObject) t;
                t = games.get("bitrate0");
                links[1] = String.valueOf(t);
                return links;
            }else if (jsonText.equals("down")) {
                links[0]="0";
                return links;
            } else {
                    links[0] = "";
                    links[1] = "";
                    return links;
                }

        } catch (org.json.simple.parser.ParseException | NullPointerException e) {

            links[0] = "";
            links[1] = "";
            return links;
        }
    }

    public ArrayList getTdyGms() throws ParseException {

        Timezone tz = new Timezone();
        ArrayList j = new ArrayList<String>();
        String gameInfo;

        for (int i = 0; i < gameinfo.size(); i++) {
            //System.out.println("looping");
            if (gameinfo.get(i).get(0) != null) {
                gameinfo.get(i).set(1, tz.toLocalTZ(gameinfo.get(i).get(1).toString()));
                gameInfo = String.format("%-3s%5s%8s%10s", gameinfo.get(i).get(2), "@", gameinfo.get(i).get(3), gameinfo.get(i).get(1).toString().substring(9));
                j.add(gameInfo);
            }
        }
        return j;
    }

    public String getGameID(int id) {
        try {
            return gameinfo.get(id).get(0).toString();
        } catch (Exception e) {
            System.err.println(e);
            return null;
        }
    }
    
    public String GetHomeTeam(int id) {
        return gameinfo.get(id).get(3).toString();
    }
    
    public String GetAwayTeam(int id) {
        return gameinfo.get(id).get(2).toString();
    }
    
    public String GetTime(int id) {
        return gameinfo.get(id).get(1).toString();
    }

    private void sort(ArrayList<ArrayList<String>> gameinfo) throws ParseException {
        Timezone tz = new Timezone();
        
        boolean flag=true;
        String t1,t2;
        
        while ( flag )
        {
               flag= false;    //set flag to false awaiting a possible swap
               for(int j=0;  j < gameinfo.size() -1;  j++ )
               {
                   if(j+1==gameinfo.size())
                       break;
                     t1=gameinfo.get(j).get(1).toString();
                     t2=gameinfo.get(j+1).get(1).toString();
                   if (tz.compareTime(t1, t2))  
                      {
                              Collections.swap(gameinfo, j, j+1);
                             flag = true;              //shows a swap occurred  
                     } 
               } 
       }
    }

    public String[] getReplayM3U8( String gid) {
        String[] links = new String[2];
        try {
            
            JSONParser parser = new JSONParser();

            String jsonText = getText("http://smb.cdnak.neulion.com/fs/nhl/mobile/feed_new/data/streams/"+getSeason().substring(0, 4)+"/ipad/" + gid.substring(4, 6) + "_" + gid.substring(6) + ".json");
            
            if (!jsonText.equals("down") || !jsonText.equals("")) {
                Object t = parser.parse(jsonText);
                JSONObject games = (JSONObject) t;
                t = games.get("gameStreams");
                games = (JSONObject) t;
                t = games.get("ipad");
                
                games = (JSONObject) t;
                Object t1=t;
                JSONObject games2 = (JSONObject) t1;
                try {
                t1 = games.get("away");
                games2 = (JSONObject) t1;
                    
                try {
                    t1 = games2.get("vod-whole");
                }
                catch (Exception e) {
                    t1 = games2.get("vod-condensed");
                }
                games2 = (JSONObject) t1;
                t1 = games2.get("bitrate0");
                links[0] = String.valueOf(t1);
                }
                catch (Exception e) {
                    links[0]="";
                }
                t = games.get("home");
                //System.out.println(t);
                games = (JSONObject) t;
                
                t = games.get("vod-whole");
                if (t==null) {
                    t = games.get("vod-condensed");
                    
                }
                games = (JSONObject) t;
                t = games.get("bitrate0");
                links[1] = String.valueOf(t);
                return links;
            }else if (jsonText.equals("down")) {
                links[0]="0";
                return links;
            } else {
                    links[0] = "";
                    links[1] = "";
                    return links;
                }

        } catch (org.json.simple.parser.ParseException | NullPointerException e) {
            System.err.println(e);
            links[0] = "";
            links[1] = "";
            return links;
        }
    }
    
    private String getSeason() {
        String year = date.substring(0, 4), month = date.substring(4, 6);
        
        int y = Integer.parseInt(year), m = Integer.parseInt(month);
        
        if (m>=1 && m<=8)
            y--;
        m=y++;
        return ""+m+y;
    }
}
